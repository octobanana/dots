#!/usr/bin/env bash

set -e

if ! [ -f ~/.vim/autoload/plug.vim ]; then
  printf "Preparing to install Vim-Plug..."
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
else
  printf "Vim-Plug already installed."
fi

