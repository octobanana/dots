" Author: Brett Robinson
" Description: My vimrc file.

"------------------------------------------------------------
" Plugins
"------------------------------------------------------------
set nocompatible
filetype off
call plug#begin('~/.vim/plugged')

" ----------------------------------
" General
" ----------------------------------
" Plug 'natebosch/vim-lsc'

"learn to make custom snippets
"change save keybind
"change lint/errors keybind
"async linting
Plug 'w0rp/ale', {'for': ['rust', 'go']}
" Plug 'vimwiki/vimwiki'
" Plug 'sirver/ultisnips'

Plug 'scrooloose/syntastic', {'for': ['c', 'cpp']}
Plug 'shougo/neocomplete.vim'
Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'
Plug 'honza/vim-snippets'

Plug 'othree/html5.vim'
Plug 'digitaltoad/vim-pug'

Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'majutsushi/tagbar'
Plug 'easymotion/vim-easymotion'
" Plug 'mattn/emmet-vim' " Insert Mode <C-Y>
Plug 'jiangmiao/auto-pairs'
Plug '~/.vim/bundle/onedark.vim'

" Python
Plug 'davidhalter/jedi-vim', {'for': 'python'}
Plug 'klen/python-mode', {'for': 'python'}

" Plug 'raimondi/delimitmate'
" Plug 'ervandew/supertab'
Plug 'christoomey/vim-tmux-navigator'
" Plug 'metakirby5/codi.vim'
Plug 'arecarn/crunch.vim'
Plug 'arecarn/selection.vim'
Plug 'luochen1990/rainbow'
" Plug 'rhysd/vim-crystal'
Plug 'asciidoc/vim-asciidoc'
" Plug 'DrawIt'
" Plug 'severin-lemaignan/vim-minimap'
" Plug 'altercation/vim-colors-solarized'
Plug 'kiddos/malokai.vim'
" Plug 'freitass/todo.txt-vim'
" Plug 'jceb/vim-orgmode'
Plug 'sickill/vim-pasta'
Plug 'junegunn/fzf', {'dir': '~/.fzf', 'do': './install --all'}
Plug 'junegunn/vim-easy-align'
" Plug 'terryma/vim-multiple-cursors'
" Plug 'jamessan/vim-gnupg'
" Plug 'mileszs/ack.vim'
" Plug 'scrooloose/nerdcommenter'
Plug 'bronson/vim-trailing-whitespace'
Plug 'nathanaelkane/vim-indent-guides'
" Plug 'gregsexton/gitv'
Plug 'scrooloose/nerdtree'
Plug 'bling/vim-airline'
" Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'godlygeek/tabular'
" Plug 'flazz/vim-colorschemes'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'kshenoy/vim-signature'
Plug 'mbbill/undotree'
Plug 'szw/vim-ctrlspace'
" Plug 'rking/ag.vim'
" Plug 'quabug/vim-gdscript'
Plug 'atom/one-dark-syntax'
Plug 'rakr/vim-one'
" Plug 'joshdick/onedark.vim'
Plug 'henrik/vim-open-url'
Plug 'tpope/vim-speeddating'
" Plug 'shougo/deoplete.nvim'
" ----------------------------------

" ----------------------------------
" C++
" ----------------------------------
" Plug 'taglist.vim'
Plug 'octol/vim-cpp-enhanced-highlight'
" ----------------------------------

" ----------------------------------
" Rust
" ----------------------------------
" Plug 'dan-t/rusty-tags', {'for': 'rust'}
" Plug 'timonv/vim-cargo', {'for': 'rust'}
Plug 'mattn/webapi-vim', {'for': 'rust'}
Plug 'rust-lang/rust.vim', {'for': 'rust'}
Plug 'racer-rust/vim-racer', {'for': 'rust'}
" if has('nvim')
"   Plug 'shougo/deoplete.nvim'
"   Plug 'sebastianmarkow/deoplete-rust', {'for': 'rust'}
" endif
" Plug 'ryanoasis/vim-devicons'
" Plug 'danilo-augusto/vim-afterglow'
" ----------------------------------

" ----------------------------------
" Ruby on Rails
" ----------------------------------
" Plug 'vim-ruby/vim-ruby'
" Plug 'tpope/vim-rails'
" Plug 'tpope/vim-endwise'
" Plug 'tpope/vim-bundler'
" Plug 'tpope/vim-haml'
" Plug 'tpope/vim-rake'
" Plug 'thoughtbot/vim-rspec'
" Plug 'xuyuanp/nerdtree-git-plugin'
" Plug 'shmay/vim_html_to_haml'
" Plug 'slim-template/vim-slim'
" Plug 'tpop/vim-dispatch'
" Plug 'kchmck/vim-coffee-script'
" ----------------------------------

" ----------------------------------
"  YCM Plugin & Hook
" ----------------------------------
" function! BuildYCM(info)
"   if a:info.status == 'installed' || a:info.force
"     !./install.py --clang-completer --racer-completer
"   endif
" endfunction
" Plug 'Valloric/YouCompleteMe', { 'do': function('BuildYCM')  }
" Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

" ----------------------------------
"  Unused
" ----------------------------------
" Plug 'ajh17/VimCompletesMe'
" Plug 'valloric/youcompleteme'
" Plug 'rip-rip/clang_complete'
" Plug 'yggdroot/indentline'
" Plug 'Rainbow-Parenthsis-Bundle'
" Plug 'kien/rainbow_parentheses.vim'
" Plug 'tpope/vim-afterimage'
" Plug 'sheerun/vim-polyglot'
" Plug 'wkentaro/conque.vim'
" Plug 'gorodinskiy/vim-coloresque'
" Plug 'KabbAmine/vCoolor.vim'
" Plug 'tpope/vim-unimpaired'
" Plug 'rizzatti/dash.vim'
" Plug 'scwood/vim-hybrid'
" Plug 'vim-scripts/sift'
" Plug 'tomsik68/vim-crystallite'
" Plug 'vim-scripts/Perfect-Dark'
" Plug 'pangloss/vim-javascript'
" Plug 'honza/vim-snippets'
" Plug 'tpope/vim-markdown'
" Plug 'klen/python-mode'
" Plug 'thinca/vim-quickrun'
" Plug 'sjl/gundo.vim'
" Plug 'shougo/unite.vim'
" Plug 'mhinz/vim-startify'
" Plug 'plasticboy/vim-markdown'
" Plug 'kien/ctrlp.vim'
" Plug 'tomtom/tcomment_vim'
" Plug 'lokaltog/vim-powerline'
" Plug 'https://github.com/itchyny/lightline.vim'
" Plug 'severin-lemaignan/vim-minimap'
" ----------------------------------

call plug#end()
filetype plugin indent on

"------------------------------------------------------------
" Options
"------------------------------------------------------------
" Allow crontab files to be edited with vim
au FileType crontab setlocal bkc=yes
if has("persistent_undo")
  set undodir=$HOME/.vim/undo
  set undofile
endif
if !has('nvim')
  set ttymouse=xterm2
endif
set mouse=a
set ttyfast
set autoread
set hidden
set wildmenu
set wildmode=longest:full,full
set wildignorecase
set visualbell
set showcmd
set sm
set ruler
set confirm
set nostartofline
set pastetoggle=<F11>
set backspace=indent,eol,start
set ttimeout
set ttimeoutlen=300
set laststatus=2
set cmdheight=2
set scrolloff=1
" set foldmethod=indent

" prevents split panes from auto resizing to equal
set noea

"------------------------------------------------------------
" Style Options
"------------------------------------------------------------
syntax enable
set t_Co=256
set t_ut=
" set t_8b=[48;2;%lu;%lu;%lum
" set t_8f=[38;2;%lu;%lu;%lum
set termguicolors
" set background=dark
" let g:one_allow_italics = 1
set cursorline
set cursorcolumn
" hi CursorLine term=bold cterm=bold guibg=Grey40
" hi CursorLine term=bold cterm=NONE ctermbg=8 ctermfg=NONE
" hi CursorColumn term=bold cterm=NONE ctermbg=8 ctermfg=NONE
" hi Normal guifg=NONE guibg=NONE ctermfg=233 ctermbg=233
if exists('+colorcolumn')
  set colorcolumn=80
endif
colorscheme onedark
let g:airline_theme='onedark'

" colorscheme myonedark
" let g:airline_theme='myonedark-airline'

" soft wrap at 80 lines
" set textwidth=80
" set wrap

" if !exists('g:airline_symbols')
"   let g:airline_symbols = {}
" endif
" let g:airline_powerline_fonts = 1

"------------------------------------------------------------
" Search Options
"------------------------------------------------------------
set hlsearch
set incsearch
set ignorecase
set smartcase

"------------------------------------------------------------
" Gutter Options
"------------------------------------------------------------
set number

"------------------------------------------------------------
" Indentation Options
"------------------------------------------------------------
set autoindent
set smartindent
set shiftwidth=2
set tabstop=2
set softtabstop=2
set expandtab
set smarttab

" Don't update folds in insert mode
aug NoInsertFolding
 	au!
 	au InsertEnter * let b:oldfdm = &l:fdm | setl fdm=manual
 	au InsertLeave * let &l:fdm = b:oldfdm
aug END

" -----------------------------------------------------------
" Auto reload file on change
" -----------------------------------------------------------
set autoread
au CursorHold * checktime

"------------------------------------------------------------
" Mappings
"------------------------------------------------------------
" vim like bindings for split pane nav
" nnoremap <c-j> <c-w>j
" nnoremap <c-k> <c-w>k
" nnoremap <c-h> <c-w>h
" nnoremap <c-l> <c-w>l

" nnoremap Y "ayiw
" nnoremap "p e"ap
" nnoremap "P b"aP

" swap q and . commands
" nnoremap . @
nnoremap , @

" nnoremap C cc<C-[>
nmap G Gzz
nmap n nzz
nmap N Nzz
nmap } }zz
nmap { {zz
" Insert mode cursor navigation
inoremap <C-F> <right>
inoremap <C-B> <left>
inoremap <C-E> <C-[>$A
inoremap <C-D> <C-[>lxi
inoremap <C-A> <C-[>^i
" Better bookmarks
nnoremap ' `
" keep visual mode after indent
vnoremap > >gv
vnoremap < <gv
" Make using surround plug in visual mode better
vmap s S
" Make . work with visually selected lines
vnoremap . :norm.<CR>
" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %
" center cursor after jumping to mark
:map <expr> M printf('`%c zz',getchar())

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" numbertoggle
nnoremap <silent> <C-n> :set relativenumber!<cr>

" -----------------------------------------------------------
" Map Leader Key and Commands
" ------------------------------------------------------------
let mapleader = ";"

inoremap <leader>a <esc>
inoremap <leader>q <esc>
nnoremap <leader>q <esc>
vnoremap <leader>q <esc>

" nnoremap <leader><leader> :
nnoremap <leader>a :
vnoremap <leader>a :

nnoremap <leader>w :w<CR>

nnoremap <leader>l :nohl<CR><C-L>

nnoremap <leader>z zA
nnoremap <leader>Za zR
nnoremap <leader>Zs zM

nnoremap <leader>fe :enew<cr>
nnoremap <leader>fj <C-^>
nnoremap <leader>ff :CtrlPBuffer<cr>
nnoremap <leader>fr :CtrlPMRU<cr>
nnoremap <leader>fg :CtrlP<cr>
nnoremap <leader>ft :CtrlPMixed<cr>
nnoremap <leader>fs :bprevious<cr>
nnoremap <leader>fd :bnext<cr>
nnoremap <leader>fq :bp <BAR> bd #<cr>
" nnoremap leader>fq :bd<cr

nnoremap <leader>je :tabnew<CR>
nnoremap <leader>jq :q<CR>
nnoremap <leader>jk :tabnext<CR>
nnoremap <leader>jj :tabprevious<CR>
nnoremap <leader>jh :tabm -1<CR>
nnoremap <leader>jl :tabm +1<CR>

" nnoremap <leader>yy viw"ay
" vnoremap <leader>yy viw"ay
" nnoremap <leader>pp "ap
" vnoremap <leader>pp "ap
" nnoremap <leader>pP "aP
" nnoremap <leader>vv V"ay
" vnoremap <leader>vv "ay

" nnoremap <leader>yy viw"+y
" vnoremap <leader>yy "+y
" nnoremap <leader>cp viw"+p
" nnoremap <leader>pp "+p
" nnoremap <leader>pP "+P
" nnoremap <leader>vv V"+y

nnoremap <F5> /<C-r><C-w><CR>
nnoremap <F6> :%s/<C-r><C-w>/

nnoremap <leader>vc :e ~/.vimrc<CR>
nnoremap <leader>vx :so $MYVIMRC<CR><C-L>

nnoremap <leader>url :OpenURL<CR>
" nnoremap <leader>up :silent !vivaldi-stable %<CR><C-L>
" nnoremap <leader>ca :silent !asciidoctor %<CR><C-L>

" for nasm files
nnoremap <leader>nasm :set syntax=nasm<cr>:SyntasticCheck nasm<cr>:set filetype=nasm<cr>:SyntasticCheck nasm<cr>

nnoremap <leader>s :SyntasticCheck gcc<cr>
nnoremap <leader>tt :TagbarToggle<CR>
vnoremap <leader>ee :Crunch<CR>
nnoremap <leader>cc :!clear && c++ -Wall -o prog % && ./prog
vnoremap <leader>aa :Tabularize /
" nnoremap <leader>s  :CtrlSpace<CR>
" nnoremap <leader>ch :VCoolor<CR>
" nnoremap <leader>cc :Commentary<CR>
nnoremap <leader>nn :NERDTreeTabsToggle<CR>
nnoremap <leader>nf :NERDTreeFocusToggle<CR>
nnoremap <leader>uu :UndotreeToggle<CR>:UndotreeFocus<CR>
" nnoremap <leader>ii :IndentLinesToggle<CR>
" nnoremap <leader>sh :ConqueTermTab bash<CR>
nnoremap <leader>mf :SignatureListBufferMarks<CR>
nnoremap <leader>ld :SignatureListGlobalMarks<CR>
nnoremap <leader>e  :Errors<CR><C-W>j
nnoremap <leader>ii :IndentGuidesToggle<CR>
nnoremap <leader>rp :RainbowToggle<CR>
" nnoremap <leader>jj zA

nnoremap <leader>gg :Gitv<CR>
nnoremap <leader>gf :Gitv!<CR>
nnoremap <leader>gc :Gcommit<CR>
nnoremap <leader>gl :Glog<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>ge :Gedit<CR>
nnoremap <leader>gb :Gbrowse<CR>
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gp :Gpush<CR>
nnoremap <leader>gP :Gpull<CR>

nnoremap <leader>hex :setlocal display=uhex<CR>
nnoremap <leader>bin :%!xxd -b<CR>
" nnoremap <leader>r :%!xxd -r<CR>

" Pretty print json in the current buffer
nnoremap <leader>mp :%! python -m json.tool<CR>

" -----------------------------------------------------------
" Buffer Management
" -----------------------------------------------------------
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" Setup some default ignores
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site)$',
  \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg|DS_Store)$',
  \}

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in version
" control. It also supports works with .svn, .hg, .bzr.
let g:ctrlp_working_path_mode = 'r'

" -----------------------------------------------------------
" CtrlSpace Settings
" -----------------------------------------------------------
" if executable("ag")
"   let g:CtrlSpaceGlobCommand = 'ag -lu --nocolor -g ""'
" endif
let g:CtrlSpaceSearchTiming = 500
let g:airline_exclude_preview = 1
let g:CtrlSpaceSaveWorkspaceOnExit = 0
let g:CtrlSpaceSaveWorkspaceOnSwitch = 0

" -----------------------------------------------------------
" FZF Settings
" -----------------------------------------------------------
" set rtp+=/usr/local/opt/fzf

" -----------------------------------------------------------
" NerdTree Settings
" -----------------------------------------------------------
let g:NERDTreeWinPos = "right"
let g:NERDTreeWinSize = 16
" -----------------------------------------------------------

" -----------------------------------------------------------
" Set Special File Types
" -----------------------------------------------------------
au BufReadPost *.gd setl syntax=gdscript
au BufReadPost *.gd setl sw=4 ts=4 sts=0 noexpandtab
au BufReadPost makefile setl sw=4 ts=4 sts=0 noexpandtab
au BufReadPost Makefile setl sw=4 ts=4 sts=0 noexpandtab

" -----------------------------------------------------------
" YCM Settings
" -----------------------------------------------------------
" let g:loaded_youcompleteme = 1

" let g:ycm_register_as_syntastic_checker = 1 "default 1
" let g:Show_diagnostics_ui = 1 "default 1

" "will put icons in Vim's gutter on lines that have a diagnostic set.
" "Turning this off will also turn off the YcmErrorLine and YcmWarningLine
" "highlighting
" let g:ycm_enable_diagnostic_signs = 0
" let g:ycm_enable_diagnostic_highlighting = 0
" let g:ycm_always_populate_location_list = 0 "default 0
" let g:ycm_open_loclist_on_ycm_diags = 0 "default 1

" let g:ycm_autoclose_preview_window_after_completion = 1
" let g:ycm_autoclose_preview_window_after_insertion = 1
" let g:ycm_add_preview_to_completeopt = 0

" let g:ycm_complete_in_comments = 1
" let g:ycm_complete_in_strings = 1 "default 1
" let g:ycm_collect_identifiers_from_tags_files = 0 "default 0

" let g:ycm_server_use_vim_stdout = 0 "default 0 (logging to console)
" let g:ycm_server_log_level = 'info' "default info

" let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'  "where to search for .ycm_extra_conf.py if not found
" let g:ycm_confirm_extra_conf = 1

" let g:ycm_goto_buffer_command = 'same-buffer' "[ 'same-buffer', 'horizontal-split', 'vertical-split', 'new-tab' ]
" let g:ycm_filetype_whitelist = { '*': 1 }

" let g:ycm_auto_trigger = 0
" let g:ycm_key_invoke_completion = '<tab>'

" nnoremap <F8> :YcmForceCompileAndDiagnostics <CR>

" -----------------------------------------------------------
" Auto-Pairs Settings
" -----------------------------------------------------------
let g:AutoPairsCenterLine = 0
" Try to fix AutoPairsReturn bug when new line is entered after bracket
" let g:AutoPairsMapCR = 0
" imap <silent><CR> <CR><Plug>AutoPairsReturn

" -----------------------------------------------------------
" vim-open-url settings
" -----------------------------------------------------------
let g:open_url_browser="vivaldi-stable"

" -----------------------------------------------------------
" NeoVim Specific Settings
" -----------------------------------------------------------
if (has("nvim"))
endif

" -----------------------------------------------------------
" Indent Guides Settings
" -----------------------------------------------------------
let g:indent_guides_start_level = 1
let g:indent_guides_guide_size  = 1
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg='#2c323c' cterm=NONE ctermbg=8 ctermfg=NONE
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg='#2c323c' cterm=NONE ctermbg=8 ctermfg=NONE
let g:indent_guides_enable_on_vim_startup = 1

" -----------------------------------------------------------
" Rainbow Parenthesis
" -----------------------------------------------------------
let g:rainbow_active = 1

" -----------------------------------------------------------
" Rust Settings
" -----------------------------------------------------------
let g:rustfmt_autosave = 1
let g:racer_cmd = "~/.cargo/bin/racer"
" let g:racer_experimental_completer = 1
" let g:ycm_rust_src_path = "~/code/github/rust/src/"

au FileType rust nnoremap <leader>rd <Plug>(rust-def)
au FileType rust nnoremap <leader>rs <Plug>(rust-def-split)
au FileType rust nnoremap <leader>rx <Plug>(rust-def-vertical)
au FileType rust nnoremap <leader>ra <Plug>(rust-doc)

" -----------------------------------------------------------
" Syntastic Settings
" -----------------------------------------------------------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_mode_map = {
  \ "mode": "active",
  \ "active_filetypes": [],
  \ "passive_filetypes": ["cpp"] }
let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
" let g:syntastic_c_include_dirs = ['~/.pebble-sdk/SDKs/4.3/sdk-core/pebble/basalt/include']
let g:syntastic_cpp_include_dirs = ["src", "/usr/include/qt/QtCore", "/usr/include/qt/QtWidgets"]
let g:syntastic_c_check_header = 1
let g:syntastic_cpp_check_header = 1
" let g:syntastic_cpp_config_file = '.vim-syntastic-includes'
" :command Sd SyntasticToggleMode

" -----------------------------------------------------------
" Slim Settings
" -----------------------------------------------------------
" autocmd BufNewFile,BufRead *.slim setlocal filetype=slim

" -----------------------------------------------------------
" C++ Settings
" -----------------------------------------------------------
autocmd FileType cpp setlocal commentstring=//%s
autocmd FileType cc  setlocal commentstring=//%s
autocmd FileType hh  setlocal commentstring=//%s
autocmd FileType c   setlocal commentstring=//%s
autocmd FileType h   setlocal commentstring=//%s

" -----------------------------------------------------------
" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif
" -----------------------------------------------------------

" -----------------------------------------------------------
" Python Settings
" -----------------------------------------------------------
" python_mode
let g:pymode_python = 'python3'

" jedi
let g:jedi#goto_command = "<leader>pc"
let g:jedi#goto_assignments_command = "<leader>pa"
let g:jedi#goto_definitions_command = "<leader>pd"
let g:jedi#documentation_command = "<leader>po"
let g:jedi#usages_command = "<leader>pu"
let g:jedi#completions_command = "<leader><Space>"
let g:jedi#rename_command = "<leader>pr"

" -----------------------------------------------------------
" Ultisnips
" -----------------------------------------------------------
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
" let g:UltiSnipsExpandTrigger="<c-m>"
" let g:UltiSnipsJumpForwardTrigger="<c-,>"
" let g:UltiSnipsJumpBackwardTrigger="<c-.>"

" If you want :UltiSnipsEdit to split your window.
" let g:UltiSnipsEditSplit="horizontal"

" -----------------------------------------------------------
" NeoComplete
" -----------------------------------------------------------
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 2

" Define dictionary.
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default' : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist',
    \ 'scheme' : $HOME.'/.gosh_completions'
        \ }

" Define keyword.
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'

" Plugin key-mappings.
inoremap <expr><C-g>     neocomplete#undo_completion()
inoremap <expr><C-l>     neocomplete#complete_common_string()

" Recommended key-mappings.
" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
  " For no inserting <CR> key.
  "return pumvisible() ? "\<C-y>" : "\<CR>"
endfunction
" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
" Close popup by <Space>.
"inoremap <expr><Space> pumvisible() ? "\<C-y>" : "\<Space>"

" AutoComplPop like behavior.
"let g:neocomplete#enable_auto_select = 1

" Shell like behavior(not recommended).
"set completeopt+=longest
"let g:neocomplete#enable_auto_select = 1
"let g:neocomplete#disable_auto_complete = 1
"inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags

" Enable heavy omni completion.
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif

let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'
"let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'

" -----------------------------------------------------------
" Neosnippets
" -----------------------------------------------------------

imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" Enable snipMate compatibility feature.
let g:neosnippet#enable_snipmate_compatibility = 1

" Tell Neosnippet about the other snippets
let g:neosnippet#snippets_directory='~/.vim/plugged/vim-snippets/snippets'

" -----------------------------------------------------------
" Ale
" -----------------------------------------------------------
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = 0
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 0
let g:ale_open_list = 0
let g:ale_keep_list_window_open = 0

" -----------------------------------------------------------
" Clang Format
" -----------------------------------------------------------
map <leader>cf :pyf /usr/share/clang/clang-format.py<cr>

" -----------------------------------------------------------
" Vue
" -----------------------------------------------------------
autocmd FileType vue setlocal syntax=html
autocmd BufNewFile,BufRead *.vue set syntax=html

" -----------------------------------------------------------
" vim-lsc language server client
" -----------------------------------------------------------
" let g:lsc_server_commands = {'c': 'clangd'}
" let g:lsc_server_commands = {'cpp': 'clangd'}
" let g:lsc_enable_autocomplete = v:true
" let g:lsc_auto_map = v:true " Use defaults
" ... or set only the keys you want mapped, defaults are:
" let g:lsc_auto_map = {
"     \ 'GoToDefinition': '<C-]>',
"     \ 'FindReferences': 'gr',
"     \ 'ShowHover': 'K',
"     \ 'Completion': 'completefunc',
"     \}

" -----------------------------------------------------------
" x
" -----------------------------------------------------------
