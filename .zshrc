# set display
export DISPLAY=:0

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# plugins=(git vi-mode history-substring-search)
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set key mode to vi
# bindkey -v
bindkey "^?" backward-delete-char

# node global packages
export PATH=~/.npm-global/bin:$PATH

# cpp aliases / functions
function cppnew() {
  if [[ $# == 1 ]]; then
    cp -r ~/code/cpp/new-project ./${1}
    cd ./${1}
  else
    printf "Error: not a valid arg\n"
  fi
}

# aliases
alias nnet="sg nnet ${@}"
alias em="emacs -nw"
alias prn="perl-rename"
alias prnn="perl-rename -n"
alias gclone="git clone"
alias vivaldi="vivaldi-stable"
alias msfconsole="msfconsole --quiet -x \"db_connect ${USER}@msf\""
alias mutt="mutt -F ~/mutt/.muttrc"
alias v="vim -u ~/.vimrc.min"
alias vi="vim -u NONE"
alias stdvim="vim -"
alias rst="stty sane"
alias zp="vim ~/.zshrc"
alias zr="source ~/.zshrc"
alias d="cd"
alias ..="cd .."
alias c="clear"
alias dc="cd && clear"
function tree_list {
  tree -i --dirsfirst -C -L 1 | sed '1d;$d;/^$/d'
}
alias utime="date +%s | cut -c-10 | clipcopy"
alias l="tree_list"
alias lt="tree -t --dirsfirst -C -L 1"
alias lc="tree -c --dirsfirst -C -L 1"
alias l2="tree --dirsfirst -C -L 2"
alias l3="tree --dirsfirst -C -L 3"
alias ll="ls --color=auto -Al"
alias ls='ls --color=auto -h --group-directories-first'
alias mp="mpsyt"
alias dl="youtube-dl"
alias ra="ranger"
alias e="emacs -nw"
alias :q="exit"
alias f="grep -i"
function duck() {
  if [[ $# == 0 ]]; then
    links "https://start.duckduckgo.com/lite/"
  else
    links "https://start.duckduckgo.com/lite/?q=${1}"
  fi
}

function myip()
{
  MYIP=$(curl -s https://api.ipify.org)
  printf "${MYIP}\n"
}

function isdown()
{
  curl -s http://downforeveryoneorjustme.com/"$@"|grep -e "It.*\."|sed 's/<\/span>//;s/\(.*[\.!]\).*<a.*>\(.*\)<\/a>\(.*\)/\1 \2\3/;s/^ *//';
}

function asm()
{
  if [[ $# > 0 ]]; then
    nasm -f elf -g -F stabs ${1}.asm && ld -m elf_i386 ${1}.o -o ${1} && ./${1}
    # nasm -f elf64 ${1}.asm && ld ${1}.o -o ${1} && ./${1}
  fi
}

function steg()
{
  if [[ $# > 0 ]]; then
    if [[ $1 == "-x" ]]; then
      steghide extract -sf $2
    elif [[ $1 == "-i" ]]; then
      steghide info $2
    elif [[ $1 == "-c" ]]; then
      steghide embed -ef $2 -cf $3
    fi
  fi
}

function snif()
{
  if [[ $# == 1 ]]; then
    grep -rnw "./" -e "${1}"
  elif [[ $# == 2 ]]; then
    grep -rnw "${1}" -e "${2}"
  else
    printf "Error: not a valid arg\n"
  fi
}

function sniff()
{
  if [[ $# == 1 ]]; then
    find ./ -name "${1}" -type f
  elif [[ $# == 2 ]]; then
    find "${1}" -name "${2}" -type f
  else
    printf "Error: not a valid arg\n"
  fi
}

alias pacman="pacman --color auto"
alias pac="sudo pacman"
alias pacml="sudo reflector --verbose -l 5 --sort rate --save /etc/pacman.d/mirrorlist"
alias pacmr="sudo pacman -Syyu"
alias pacua="sudo pacman -Syu && pacman -Qqe > ~/.paclist.txt"
alias pacul="sudo pacman -Sy"
alias pacup="sudo pacman -Sy && pacman -Qu"
alias pacdo="sudo pacman -S"
alias pacdc="sudo pacman -U"
alias pacsg="sudo pacman -Ss"
alias pacsi="sudo pacman -Si"
alias pacsl="sudo pacman -Qs"
alias pacsu="sudo pacman -Qu"
alias pacsv="list-pac-versions"
alias pacsc="ls /var/cache/pacman/pkg | grep -i"
alias pacil="sudo pacman -Qi"
alias pacig="sudo pacman -Si"
alias pacfl="sudo pacman -Qlq"
alias pacrp="sudo pacman -R"
alias pacrm="sudo pacman -Rs"
alias pacrf="sudo pacman -Rdd"
alias pacc="sudo pacman -Sc"
alias paccc="sudo pacman -Scc"

alias aur="pacaur"
alias aurup="pacaur -Syu"
alias aurdo="pacaur -S"
alias aursg="pacaur -Ss"
alias aursi="pacaur -Si"
alias aursl="pacaur -Qs"
alias aursu="sudo pacman -Qu"

function list-pac-versions {
  for i in $(pacman -Qqu)
  do
    printf "$i: "
    printf "$(pacman -Qi "$i" | grep 'Version' | sed 's/^Version\s*:\s//') "
    echo   "$(pacman -Si "$i" | grep 'Version' | sed 's/^Version\s*:\s//')"
  done
}

function gitsize()
{
  if [[ $# > 0 ]]; then
    curl https://api.github.com/repos/${1} | grep -i size
  else
    printf "Error: expected <user>/<repo>\n"
  fi
}

function mkcd()
{
  case "$1" in
    */..|*/../) cd -- "$1";; # that doesn't make any sense unless the directory already exists
    /*/../*) (cd "${1%/../*}/.." && mkdir -p "./${1##*/../}") && cd -- "$1";;
    /*) mkdir -p "$1" && cd "$1";;
    */../*) (cd "./${1%/../*}/.." && mkdir -p "./${1##*/../}") && cd "./$1";;
    ../*) (cd .. && mkdir -p "${1#.}") && cd "$1";;
    *) mkdir -p "./$1" && cd "./$1";;
  esac
}

# set variables
# export DISPLAY=x
export EDITOR="vim"
# export MANPAGER="/bin/sh -c \"col -b | vim -c 'set ft=man ts=8 nomod nolist nonu noma' -\""
export MANPAGER="/bin/sh -c \"col -b | vim -u NONE -c 'set ft=man ts=8 nomod nolist nonu noma' -\""
export CLICOLOR=1
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcxd
export TERM=xterm-256color
export LC_CTYPE=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export MYVIMRC=~/.vimrc
export LYNX_LSS=$HOME/.config/lynx/lynx.lss

alias ecs="cat ~/.easy_command/commands.txt | fzf | tr -d '\n' | clipcopy"

function ec-history-fzf() {
  cat ~/.zsh_history | tail -r | awk '{$1=$1};1' | awk '!x[$0]++' | grep . | fzf
}
function ec-command-sort-fzf() {
  cat ~/.easy_command/commands.txt | sort | uniq | awk '{$1=$1};1' | awk '!x[$0]++' | grep . | sort | fzf
}
ec-command-fzf()
{
  # cat ~/.command_db | grep . | sort | uniq | fzf
  LBUFFER=$(cat ~/.easy_command/commands.txt | awk '{$1=$1};1' | awk '!x[$0]++' | grep . | grep -v 'EOF' | sort | uniq | fzf)
}

zle -N ec-command-fzf
bindkey '^f^f' ec-command-fzf

bindkey -s '^[f' 'ec\n'

bindkey -s '^[r' 'ranger\n'

# run-easy-command() zle -M "$(bash ec)"
# zle -N run-easy-command
# bindkey '^f^f' run-easy-command

insert_sudo()
{
  zle beginning-of-line
  zle -U "sudo "
}
zle -N insert-sudo insert_sudo
bindkey "^[s" insert-sudo

# bindkey "\C-x\C-a" vi-movement-mode
# bindkey "\C-x\C-e" shell-expand-line
# bindkey "\C-x\C-r" reset-prompt

# bindkey "\C-f\C-a" "\C-[Iecho \"\C-[A\" >> ~/.command_db"
# bindkey "\C-f\C-d" "$EDITOR ~/.command_db\015"
# bindkey "\C-f\C-f" "\C-x\C-a$a \C-x\C-addi$(ec-command-fzf)\C-x\C-e\C-x\C-a0Px$a \C-x\C-r\C-x\C-axa"
# bindkey "\C-f\C-s" "\C-x\C-a$a \C-x\C-addi$(ec-command-sort-fzf)\C-x\C-e\C-x\C-a0Px$a \C-x\C-r\C-x\C-axa"
# bindkey "\C-f\C-h" "\C-x\C-a$a \C-x\C-addi$(ec-history-fzf)\C-x\C-e\C-x\C-a0Px$a \C-x\C-r\C-x\C-axa"


export FZF_DEFAULT_COMMAND='ag ""'
export FZF_DEFAULT_OPTS="--reverse --inline-info"
source /usr/share/fzf/completion.zsh
source /usr/share/fzf/key-bindings.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# easy-command
source ~/.easy_command/ec-function

# environment variable secrets
source ~/.envar_secrets

# C++ warning flags
# export CPPFLAGS="-pedantic -Wall -Wextra -Wcast-align -Wcast-qual
# -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self
# -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept
# -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow
# -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5
# -Wswitch-default -Wundef -Werror -Wno-unused"

# Zsh syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# zsh history
export HISTSIZE=100000
setopt HIST_IGNORE_SPACE

# Allow any key to unfreeze terminal cursor
# after pressing <C-s>
stty ixany

# set GPG TTY
export GPG_TTY=$(tty)

# Refresh gpg-agent tty in case user switches into an X Session
gpg-connect-agent updatestartuptty /bye >/dev/null
