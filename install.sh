#!/usr/bin/env bash

# Exit on error
set -e

echo "Installing Dot Files"

# copy files into home directory
rsync -a .tmux.conf ~/
rsync -a .vimrc ~/
rsync -a .zshrc  ~/
rsync -a .vim ~/
rsync -a .oh-my-zsh ~/

echo "Installing Vim Plug"
./install-vim-plug.sh

echo "Done!"
