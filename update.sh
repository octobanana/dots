#!/usr/bin/env bash

# Exit on error
set -e

echo "Updating Dot Files"

# check and pull any updates from repo
git pull

# copy files into home directory
rsync -a .tmux.conf ~/
rsync -a .vimrc ~/
rsync -a .zshrc  ~/

echo "Done!"
